#ifndef MPI_DEBUG_UTILS_H
#define MPI_DEBUG_UTILS_H

#ifdef RICH_MPI
    #include <mpi.h>
    #include "SmartCollectives.hpp"
#endif // RICH_MPI

#endif // MPI_DEBUG_UTILS_H