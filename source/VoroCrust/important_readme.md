# Used Pymesh https://pymesh.readthedocs.io/en/latest/ to create the mesh of the cat in data from an obj file.

# Formula for uniform sampling a point in a triangle https://math.stackexchange.com/questions/18686/uniform-random-point-in-triangle-in-3d

# Formula for area of face https://math.stackexchange.com/questions/3207981/how-do-you-calculate-the-area-of-a-2d-polygon-in-3d

# git repository for new kd_tree https://github.com/jtsiomb/kdtree