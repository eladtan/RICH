{
    "hdf5_include": "/software/x86_64/5.14.0/hdf5/1.14.2/gcc/12.3.0_cxx/include",
    "hdf5_lib_dir": "/software/x86_64/5.14.0/hdf5/1.14.2/gcc/12.3.0_cxx/lib/",
    
    "intel_hdf5_include": "/software/x86_64/5.14.0/hdf5/1.14.3/Intel/OneApi/2024.2.1/enable-cxx/include",
    "intel_hdf5_lib_dir": "/software/x86_64/5.14.0/hdf5/1.14.3/Intel/OneApi/2024.2.1/enable-cxx/lib",

    "vtk"         : "/software/x86_64/5.14.0/vtk/9.3.0/gcc/12.3.0/with_mesa/",
    "vtk_intel"         : "/software/x86_64/5.14.0/vtk/9.3.0/gcc/12.3.0/with_mesa/",
    # 'vtk_intel':    "/software/x86_64/5.14.0/vtk/9.3.0/Intel/OneApi/2024.2.1/with_mesa",

    'ifort'    : "/software/x86_64/5.14.0/intel/OneApi/2024.2.1/compiler/2024.2/bin/ifx",
    'icx-cc'      : "/software/x86_64/5.14.0/intel/OneApi/2024.2.1/compiler/2024.2/bin/icx-cc",
    'icx'     : "/software/x86_64/5.14.0/intel/OneApi/2024.2.1/compiler/2024.2/bin/icx",

    'gfortran' : "/software/x86_64/5.14.0/gcc/12.3.0/bin/gfortran",
    'gcc'      : "/software/x86_64/5.14.0/gcc/12.3.0/bin/gcc",
    'g++'      : "/software/x86_64/5.14.0/gcc/12.3.0/bin/g++",

    # 'mpicc_gcc'    : "/software/x86_64/5.14.0/openmpi/4.1.6/gcc/12.3.0/bin/mpicc",
    # 'mpic++_gcc'   : "/software/x86_64/5.14.0/openmpi/4.1.6/gcc/12.3.0/bin/mpic++",

    'mpicc_gcc'    : "/software/x86_64/5.14.0/openmpi/4.1.6_with_ucc/gcc/12.3.0/bin/mpicc",
    'mpic++_gcc'   : "/software/x86_64/5.14.0/openmpi/4.1.6_with_ucc/gcc/12.3.0/bin/mpic++",

    # 'mpicc_intel'    : "/software/x86_64/5.14.0/intel/OneApi/2024.2.1/mpi/latest/bin/mpiicx",
    # 'mpic++_intel'   : "/software/x86_64/5.14.0/intel/OneApi/2024.2.1/mpi/latest/bin/mpiicpx",
    'mpicc_intel'    : "/software/x86_64/5.14.0/openmpi/4.1.6/Intel/OneApi/2024.2.1/bin/mpicc",
    'mpic++_intel'   : "/software/x86_64/5.14.0/openmpi/4.1.6/Intel/OneApi/2024.2.1/bin/mpicxx",

#    'pybind11'   : "/software/x86_64/5.14.0-284.11.1.el9_2.x86_64/pybind11/2.6.1/mock_install/share/cmake/pybind11",

    'jsoncpp_include' : "/software/x86_64/5.14.0/jsoncpp/1.9.5/include",
    'jsoncpp_lib_dir' : "/software/x86_64/5.14.0/jsoncpp/1.9.5/lib64",
    'cgal_include': "/software/x86_64/5.14.0/cgal/5.6/include",

    # "qhull_include": "/home/maorm/opt/qhull/current/include",
    # "qhull_lib_dir": "/home/maorm/opt/qhull/current/lib",

    "vtune_include": "/software/x86_64/5.14.0/intel/OneApi/2024.2.1/vtune/2024.2/include",
    "vtune_lib_dir": "/software/x86_64/5.14.0/intel/OneApi/2024.2.1/vtune/2024.2/lib64",
    'pybind11'   : "/software/x86_64/5.14.0/pybind11/2.11.1/share/cmake/pybind11",

    'python_include': "/software/x86_64/5.14.0/python/3.9.18/include/python3.9/",
    'python_lib_dir': "/software/x86_64/5.14.0/python/3.9.18/lib/",
}
